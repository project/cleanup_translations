<?php
/**
 * @file
 * This module syncs the strings in the database with those defined in code and
 * allows to remove outdated strings from the database.
 */


/**
 * Implements hook_menu().
 */
function cleanup_translations_menu() {
  $items = array(
    'admin/config/regional/translate/clean' => array(
      'title' => 'Clean up',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('cleanup_translations_form'),
      'access arguments' => array('translate interface'),
      // Place it on the right side of the potx "extract" tab.
      'weight' => 210,
      'type' => MENU_LOCAL_TASK,
    ),
  );
  return $items;
}

/**
 * Page callback of admin/config/regional/translate/clean.
 *
 * @param string $step
 *   Additional path component after clean/xxx
 */
function cleanup_translations_form($form, &$form_state, $step = '') {
  if ($step == 'review') {
    return cleanup_translations_form_review($form, $form_state);
  }

  $form['explanation1'] = array(
    '#type' => 'item',
    '#markup' => t('Remove all built-in interface strings that are not present in code from the translation tables.'),
  );
  $form['explanation2'] = array(
    '#type' => 'item',
    '#markup' => t('This eases the translators work as outdated strings (for example due to typos, removed modules, etc.) are removed. It also speeds up the actual translation lookup queries as the tables get smaller.'),
  );
  $form['explanation3'] = array(
    '#type' => 'item',
    '#markup' => t('This module does not add any strings to the source nor the translation table.'),
  );
  $form['explanation4'] = array(
    '#type' => 'item',
    '#markup' => t('You can review the strings to remove before they are actually deleted.'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'cleanup' => array(
      '#type' => 'submit',
      '#value' => t('Analyse current strings in code'),
    ),
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function cleanup_translations_form_submit($form, &$form_state) {
  if (!_cleanup_translations_lock()) {
    drupal_set_message(t('Error: Somebody else seems to be using this feature. Please try again in a few minutes.'), 'error');
    return;
  }

  // Get access to the potx functions.
  module_load_include('inc', 'potx');

  // Get a list of all files.
  $files = _potx_explore_dir(DRUPAL_ROOT . '/');

  // Start with a fresh empty string table.
  db_truncate('cleanup_translations')->execute();

  // Create a batch operation.
  $operations = array();
  // Process one file per operation, let the batch API decide how many
  // operations are executed per request.
  foreach ($files as $file) {
    $operations[] = array(
      'cleanup_translations_batch_process',
      array($file),
    );
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'cleanup_translations_batch_finished',
    'title' => t('Scanning source code for strings'),
    'progress_message' => t('Processed @current out of @total files. Remaining time: @estimate.'),
    'error_message' => t('String scan has encountered an error.'),
  );

  batch_set($batch);
}

/**
 * Batch callback function.
 *
 * Scan a single file for any strings and insert them into the sync table
 * cleanup_translations.
 */
function cleanup_translations_batch_process($file, &$context) {
  // Renew the lock every batch execution.
  if (!_cleanup_translations_lock()) {
    return;
  }

  // Get access to the potx functions.
  module_load_include('inc', 'potx');

  // Scan the file for strings.
  _potx_process_file($file);

  // Transfer strings to database.
  // Prepare insert query.
  $query = db_insert('cleanup_translations')
    ->fields(array('source', 'location', 'context'));

  // Structure: $_potx_strings[$value][$context][$file][] = $line.
  global $_potx_strings;
  foreach ($_potx_strings as $string => $contexts) {
    // Strings from format_plural() are separated by a null byte, split these
    // strings and treat them as separate ones.
    $string_parts = preg_split('/\0/', $string);

    foreach ($string_parts as $string) {
      // Stip out slash escapes.
      $string = stripcslashes($string);

      // Insert a separate row for each context.
      foreach ($contexts as $ctx => $files) {
        foreach ($files as $location => $lines) {
          // Show location relative to the root directory.
          $location = str_replace(DRUPAL_ROOT . '/', '', $location);

          foreach ($lines as $line) {
            $query->values(array(
              'source' => $string,
              'location' => $location . ':' . $line,
              'context' => $ctx,
            ));
          }
        }
      }
    }
  }
  // Finally insert everything in a single operation.
  $query->execute();

  // Reset the global variable as the next batch operation may run during the
  // same request.
  $_potx_strings = array();
}

/**
 * Callback of the batch API after the processing finished.
 */
function cleanup_translations_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_goto('admin/config/regional/translate/clean/review');
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    )), 'error');
  }
}

/**
 * Display an overview of the outdated strings after the batch process finished.
 */
function cleanup_translations_form_review($form, &$form_state) {
  // Check that the cleanup_translations table is not empty, otherwise all
  // strings including their translations would be removed.
  $count = db_query('SELECT COUNT(ctid) as num_rows FROM {cleanup_translations}')->fetchField();
  if ((int) $count === 0) {
    drupal_goto('admin/config/regional/translate/clean');
  }

  // Get an extended lock (10 minutes) that allows to review results.
  _cleanup_translations_lock(600);

  $strings = _cleanup_translations_get_outdated_strings();

  // Create a table listing all strings that are to be removed.
  $header = array(
    'string' => t('String'),
    'location' => t('Location'),
    'context' => t('Context'),
  );
  $rows = array();
  foreach ($strings as $lid => $string) {
    $rows[$lid] = array(
      'string' => check_plain($string->source),
      'location' => check_plain($string->location),
      'context' => check_plain($string->context),
    );
  }

  $form['status'] = array(
    '#type' => 'item',
    '#markup' => format_plural(count($strings), 'One string has not been found in the source code files.', '@count strings have not been found in the source code files.'),
  );
  $form['status2'] = array(
    '#type' => 'item',
    '#markup' => t('Check the strings that will be removed. Orphaned translations (translations without a source string) will be removed as well.'),
  );

  $form['strings'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#multiple' => TRUE,
    '#empty' => t('The database is up-to-date, nothing to remove.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['cleanup'] = array(
    '#type' => 'submit',
    '#value' => t('Remove strings'),
    '#suffix' => t('Warning: This operation is permanent!'),
  );

  $form['#submit'][] = 'cleanup_translations_form_review_submit';

  return $form;
}


/**
 * Implements hook_submit().
 */
function cleanup_translations_form_review_submit($form, &$form_state) {
  if (!_cleanup_translations_lock()) {
    drupal_set_message(t('Error: Somebody else seems to be using this feature. Please try again in a few minutes.'), 'error');
    return;
  }

  // Filter out unchecked strings.
  $strings = array_filter($form_state['values']['strings']);

  if (count($strings) > 0) {
    db_delete('locales_source')
      ->condition('lid', array_keys($strings))
      ->execute();

    drupal_set_message(format_plural(count($strings), 'One source string has been removed from the string table.', '@count source strings have been removed from the string table.'));
  }

  // Remove any translations where no source exists.
  // Use two queries because db_delete() does not support joins.
  $query = db_select('locales_target', 't');
  $query->leftJoin('locales_source', 's', 's.lid = t.lid');
  $query->fields('t', array('lid'))
    // No matching row in table locales_target.
    ->isNull('s.lid')
    // We only need the lid once, we might get it multiple times as the table's
    // index consists of rows (lid, language, plural).
    ->groupBy('t.lid');

  $lids = $query->execute()->fetchCol();

  // Finally remove the translations.
  if (count($lids) > 0) {
    db_delete('locales_target')
      ->condition('lid', $lids)
      ->execute();

    drupal_set_message(format_plural(count($lids), 'Removed one orphaned translation from the translation table.', 'Removed @count orphaned translations from the translation table.'));
  }
  // Rebuild the form since we stay on it.
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function: Find all outdated strings in the locales_source table.
 *
 * @return array
 *   List of string info indexed by "lid".
 */
function _cleanup_translations_get_outdated_strings() {
  // Get all strings from the string table that have no equivalent row in the
  // cleanup_translations table which contains all strings in all files.
  $query = db_select('locales_source', 's');
  $query->leftJoin('cleanup_translations', 'ct', 'ct.source = s.source AND ct.context = s.context');
  $query->fields('s', array('lid', 'source', 'location', 'context'))
    // Only search interface strings.
    ->condition('s.textgroup', 'default')
    // No row in table cleanup_translations.
    ->isNull('ct.ctid');

  return $query->execute()->fetchAllAssoc('lid');
}

/**
 * Custom persistent locking mechanism.
 *
 * It is VERY important that the batch cannot be run simultaneously, otherwise
 * strings and translations might be deleted erroneously.
 * The function lock_acquire cannot be used as the lock needs to persist
 * over multiple requests, but the locks are released at the end of a request.
 *
 * @param int $timeout
 *   The number of seconds that the lock
 *
 * @param bool $release
 *   Whether to release the lock.
 *
 * @return bool
 *   Whether the lock has been acquired.
 */
function _cleanup_translations_lock($timeout = 60, $release = FALSE) {
  global $user;
  // Anonymous users cannot acquire this lock.
  if ($user->uid == 0) {
    return FALSE;
  }

  $lock = variable_get('cleanup_translations_lock');

  // Cannot acquire someone elses lock that has not expired.
  if (is_array($lock) && $lock['uid'] != $user->uid && $lock['expire'] > microtime(TRUE)) {
    return FALSE;
  }
  if ($release) {
    variable_del('cleanup_translations_lock');
    return TRUE;
  }
  // It's either our lock or it has expired.
  variable_set('cleanup_translations_lock', array(
    'uid' => $user->uid,
    'expire' => microtime(TRUE) + $timeout,
  ));
  return TRUE;
}
